//
//  AppDelegate.h
//  game
//
//  Created by Songtao He on 2/19/16.
//  Copyright © 2016 recg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

