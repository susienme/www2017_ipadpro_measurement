//
//  Shader.fsh
//  game
//
//  Created by Songtao He on 2/19/16.
//  Copyright © 2016 recg. All rights reserved.
//

/*varying lowp vec4 colorVarying;

void main()
{
    gl_FragColor = colorVarying;
}*/


precision mediump float;
uniform vec4 vColor;

void main()
{
    gl_FragColor = vColor; //colorVarying;
}
