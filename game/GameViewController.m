//
//  GameViewController.m
//  game
//
//  Created by Songtao He on 2/19/16.
//  Copyright © 2016 recg. All rights reserved.
//

#import "GameViewController.h"
#import <OpenGLES/ES2/glext.h>
//#import <SpriteKit/SpriteKit.h>
//@import SpriteKit;

//#include <vector>
//#include "Shader.h"

#define BUFFER_OFFSET(i) ((char *)NULL + (i))

// Uniform index.
enum
{
    UNIFORM_MODELVIEWPROJECTION_MATRIX,
    UNIFORM_NORMAL_MATRIX,
    NUM_UNIFORMS
};
GLint uniforms[NUM_UNIFORMS];

// Attribute index.
enum
{
    ATTRIB_VERTEX,
    ATTRIB_NORMAL,
    NUM_ATTRIBUTES
};

GLfloat gCubeVertexData[216] =
{
    // Data layout for each line below is:
    // positionX, positionY, positionZ,     normalX, normalY, normalZ,
    0.5f, -0.5f, -0.5f,        1.0f, 0.0f, 0.0f,
    0.5f, 0.5f, -0.5f,         1.0f, 0.0f, 0.0f,
    0.5f, -0.5f, 0.5f,         1.0f, 0.0f, 0.0f,
    0.5f, -0.5f, 0.5f,         1.0f, 0.0f, 0.0f,
    0.5f, 0.5f, -0.5f,          1.0f, 0.0f, 0.0f,
    0.5f, 0.5f, 0.5f,         1.0f, 0.0f, 0.0f,
    
    0.5f, 0.5f, -0.5f,         0.0f, 1.0f, 0.0f,
    -0.5f, 0.5f, -0.5f,        0.0f, 1.0f, 0.0f,
    0.5f, 0.5f, 0.5f,          0.0f, 1.0f, 0.0f,
    0.5f, 0.5f, 0.5f,          0.0f, 1.0f, 0.0f,
    -0.5f, 0.5f, -0.5f,        0.0f, 1.0f, 0.0f,
    -0.5f, 0.5f, 0.5f,         0.0f, 1.0f, 0.0f,
    
    -0.5f, 0.5f, -0.5f,        -1.0f, 0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,       -1.0f, 0.0f, 0.0f,
    -0.5f, 0.5f, 0.5f,         -1.0f, 0.0f, 0.0f,
    -0.5f, 0.5f, 0.5f,         -1.0f, 0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,       -1.0f, 0.0f, 0.0f,
    -0.5f, -0.5f, 0.5f,        -1.0f, 0.0f, 0.0f,
    
    -0.5f, -0.5f, -0.5f,       0.0f, -1.0f, 0.0f,
    0.5f, -0.5f, -0.5f,        0.0f, -1.0f, 0.0f,
    -0.5f, -0.5f, 0.5f,        0.0f, -1.0f, 0.0f,
    -0.5f, -0.5f, 0.5f,        0.0f, -1.0f, 0.0f,
    0.5f, -0.5f, -0.5f,        0.0f, -1.0f, 0.0f,
    0.5f, -0.5f, 0.5f,         0.0f, -1.0f, 0.0f,
    
    0.5f, 0.5f, 0.5f,          0.0f, 0.0f, 1.0f,
    -0.5f, 0.5f, 0.5f,         0.0f, 0.0f, 1.0f,
    0.5f, -0.5f, 0.5f,         0.0f, 0.0f, 1.0f,
    0.5f, -0.5f, 0.5f,         0.0f, 0.0f, 1.0f,
    -0.5f, 0.5f, 0.5f,         0.0f, 0.0f, 1.0f,
    -0.5f, -0.5f, 0.5f,        0.0f, 0.0f, 1.0f,
    
    0.5f, -0.5f, -0.5f,        0.0f, 0.0f, -1.0f,
    -0.5f, -0.5f, -0.5f,       0.0f, 0.0f, -1.0f,
    0.5f, 0.5f, -0.5f,         0.0f, 0.0f, -1.0f,
    0.5f, 0.5f, -0.5f,         0.0f, 0.0f, -1.0f,
    -0.5f, -0.5f, -0.5f,       0.0f, 0.0f, -1.0f,
    -0.5f, 0.5f, -0.5f,        0.0f, 0.0f, -1.0f
};

float lastX = 0.0, lastY = 0.0;
GLuint g_program;
GLuint g_vertexArray;
GLuint g_vertexBuffer;

void drawRect(float _x,float _y,float _w,float _h,float _r,float _g, float _b)
{
    int COORDS_PER_VERTEX = 2;
    int vertexStride = COORDS_PER_VERTEX * 4;
    int vertexCount = 6;
    unsigned int m_geometryBuffer;
    float triangleCoords[] = {
        _x, _y,
        _x + _w, _y,
        _x, _y + _h,
        _x, _y + _h,
        _x + _w, _y + _h,
        _x + _w, _y
    };
    
    float color[] = { _r, _g, _b, 1.0f };
    
//    NSLog(@"%f %f %f %f | %f %f %f", _x, _y, _w, _h, _r, _g, _b);
    
/*    ByteBuffer bb = ByteBuffer.allocateDirect(
                                              // (number of coordinate values * 4 bytes per float)
                                              triangleCoords.length * 4);
    // use the device hardware's native byte order
    bb.order(ByteOrder.nativeOrder());
    
    // create a floating point buffer from the ByteBuffer
    vertexBuffer = bb.asFloatBuffer();
    // add the coordinates to the FloatBuffer
    vertexBuffer.put(triangleCoords);
    // set the buffer to read the first coordinate
    vertexBuffer.position(0);*/
    
    //glGenVertexArraysOES(1, &g_vertexArray);
//    glBindVertexArrayOES(g_vertexArray);
    
//    glGenBuffers(1, &g_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, g_vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangleCoords), triangleCoords, GL_STATIC_DRAW);
    
/*    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 2, GL_FLOAT, GL_FALSE, 3*sizeof(float), BUFFER_OFFSET(0));*/
    
    /*glEnableVertexAttribArray(GLKVertexAttribNormal);
    glVertexAttribPointer(GLKVertexAttribNormal, 3, GL_FLOAT, GL_FALSE, 24, BUFFER_OFFSET(12));*/
    
    
//    GLES20.glUseProgram(mProgram);
    glUseProgram(g_program);
    
    // get handle to vertex shader's vPosition member
    int mPositionHandle = glGetAttribLocation(g_program, "vPosition");
    
    // Enable a handle to the triangle vertices
    glEnableVertexAttribArray(mPositionHandle);
    
    // Prepare the triangle coordinate data
/*    glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX,
                                 GL_FLOAT, false,
                                 vertexStride, g_vertexBuffer);*/
    glVertexAttribPointer(mPositionHandle, 2, GL_FLOAT, GL_FALSE, vertexStride, BUFFER_OFFSET(0));
    
    // get handle to fragment shader's vColor member
    int mColorHandle = glGetUniformLocation(g_program, "vColor");
    
    // Set color for drawing the triangle
//    glUniform4fv(mColorHandle, 1, color, 0);
    glUniform4fv(mColorHandle, 1, color);
    
    // Draw the triangle
    glDrawArrays(GL_TRIANGLES, 0, vertexCount);
    
    // Disable vertex array
    glDisableVertexAttribArray(mPositionHandle);
    
    
    return;
    
    /////////////////////////////////
    /*
    std::vector<float> geometryData;
    
    
    //4 floats define one vertex (x, y, z and w), first one is lower left
    geometryData.push_back(-0.5); geometryData.push_back(-0.5); geometryData.push_back(0.0); geometryData.push_back(1.0);
    //we go counter clockwise, so lower right vertex next
    geometryData.push_back( 0.5); geometryData.push_back(-0.5); geometryData.push_back(0.0); geometryData.push_back(1.0);
    //top vertex is last
    geometryData.push_back( 0.0); geometryData.push_back( 0.5); geometryData.push_back(0.0); geometryData.push_back(1.0);
    */
    //generate an ID for our geometry buffer in the video memory and make it the active one
    glGenBuffers(1, &m_geometryBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_geometryBuffer);
    
    //send the data to the video memory
    glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(float), triangleCoords, GL_STATIC_DRAW);
    
    //m_shader = new Shader("shader.vert", "shader.frag");
    
    //if(!m_shader->compileAndLink())
    ///{
    //    NSLog(@"Encountered problems when loading shader, application will crash...");
    //}
    
    //tell OpenGL to use this shader for all coming rendering
    glUseProgram(g_program);
    
    //get the attachment points for the attributes position and color
    unsigned int m_positionLocation = glGetAttribLocation(g_program, "position");
    //m_colorLocation = glGetAttribLocation(m_shader->getProgram(), "color");
    
    //check that the locations are valid, negative value means invalid
    if(m_positionLocation < 0 )
    {
        NSLog(@"Could not query attribute locations");
    }
    
    //enable these attributes
    glEnableVertexAttribArray(m_positionLocation);
    //glEnableVertexAttribArray(m_colorLocation);
    
    NSLog(@"Init done...");


    glBindBuffer(GL_ARRAY_BUFFER, m_geometryBuffer);
    //point the position attribute to this buffer, being tuples of 4 floats for each vertex
    glVertexAttribPointer(m_positionLocation, 4, GL_FLOAT, GL_FALSE, 0, NULL);
    
    //bint the color VBO
    //glBindBuffer(GL_ARRAY_BUFFER, m_colorBuffer);
    //this attribute is only 3 floats per vertex
    //glVertexAttribPointer(m_colorLocation, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    
    //initiate the drawing process, we want a triangle, start at index 0 and draw 3 vertices
//    glDrawArrays(GL_TRIANGLES, 0, 3);
    
    
    
    
    
}

@interface GameViewController () {
    GLuint _program;
    
    GLKMatrix4 _modelViewProjectionMatrix;
    GLKMatrix3 _normalMatrix;
    float _rotation;
    
    GLuint _vertexArray;
    GLuint _vertexBuffer;
}
@property (strong, nonatomic) EAGLContext *context;
@property (strong, nonatomic) GLKBaseEffect *effect;

- (void)setupGL;
- (void)tearDownGL;

- (BOOL)loadShaders;
- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file;
- (BOOL)linkProgram:(GLuint)prog;
- (BOOL)validateProgram:(GLuint)prog;
@end

@implementation GameViewController
//@synthesize view;

/*-(void)viewWillAppear:(BOOL)animated{
    CGFloat width = self.view.bounds.size.width;
    CGFloat height = self.view.bounds.size.height;
//    NSLog(@"w/h %f %f", mainViewWidth, mainViewHeight);
    glViewport(0, 0, width, height);
}*/

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];

    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
    self.preferredFramesPerSecond = 60;
//    ((SKView *) (self.view)).frameInterval = 1;
    [self setupGL];
}

- (void)dealloc
{
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

    if ([self isViewLoaded] && ([[self view] window] == nil)) {
        self.view = nil;
        
        [self tearDownGL];
        
        if ([EAGLContext currentContext] == self.context) {
            [EAGLContext setCurrentContext:nil];
        }
        self.context = nil;
    }

    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)setupGL
{
    [EAGLContext setCurrentContext:self.context];
    
    [self loadShaders];
    
//    self.effect = [[GLKBaseEffect alloc] init];
//    self.effect.light0.enabled = GL_TRUE;
//    self.effect.light0.diffuseColor = GLKVector4Make(1.0f, 0.4f, 0.4f, 1.0f);
    
//    glEnable(GL_DEPTH_TEST);
    
    /*glGenVertexArraysOES(1, &_vertexArray);
    glBindVertexArrayOES(_vertexArray);
    
    glGenBuffers(1, &_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(gCubeVertexData), gCubeVertexData, GL_STATIC_DRAW);
    
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 24, BUFFER_OFFSET(0));*/
    
    /*glEnableVertexAttribArray(GLKVertexAttribNormal);
    glVertexAttribPointer(GLKVertexAttribNormal, 3, GL_FLOAT, GL_FALSE, 24, BUFFER_OFFSET(12));*/
    
//    glBindVertexArrayOES(0);
    glGenVertexArraysOES(1, &g_vertexArray);
    glBindVertexArrayOES(g_vertexArray);
    glGenBuffers(1, &g_vertexBuffer);
}

- (void)tearDownGL
{
    [EAGLContext setCurrentContext:self.context];
    
    glDeleteBuffers(1, &_vertexBuffer);
    glDeleteVertexArraysOES(1, &_vertexArray);
    
    self.effect = nil;
    
    if (_program) {
        glDeleteProgram(_program);
        _program = 0;
    }
}

#pragma mark - GLKView and GLKViewController delegate methods

- (void)update
{
/*    float aspect = fabs(self.view.bounds.size.width / self.view.bounds.size.height);
    GLKMatrix4 projectionMatrix = GLKMatrix4MakePerspective(GLKMathDegreesToRadians(65.0f), aspect, 0.1f, 100.0f);
    
    self.effect.transform.projectionMatrix = projectionMatrix;
    
    GLKMatrix4 baseModelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, -4.0f);
    baseModelViewMatrix = GLKMatrix4Rotate(baseModelViewMatrix, _rotation, 0.0f, 1.0f, 0.0f);
    
    // Compute the model view matrix for the object rendered with GLKit
    GLKMatrix4 modelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, -1.5f);
    modelViewMatrix = GLKMatrix4Rotate(modelViewMatrix, _rotation, 1.0f, 1.0f, 1.0f);
    modelViewMatrix = GLKMatrix4Multiply(baseModelViewMatrix, modelViewMatrix);
    
    self.effect.transform.modelviewMatrix = modelViewMatrix;
    
    // Compute the model view matrix for the object rendered with ES2
    modelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, 1.5f);
    modelViewMatrix = GLKMatrix4Rotate(modelViewMatrix, _rotation, 1.0f, 1.0f, 1.0f);
    modelViewMatrix = GLKMatrix4Multiply(baseModelViewMatrix, modelViewMatrix);
    
    _normalMatrix = GLKMatrix3InvertAndTranspose(GLKMatrix4GetMatrix3(modelViewMatrix), NULL);
    
    _modelViewProjectionMatrix = GLKMatrix4Multiply(projectionMatrix, modelViewMatrix);
    
    _rotation += self.timeSinceLastUpdate * 0.5f;*/
    //NSLog(@"UPDATE");
}

//- (void) drawRect: (float)_x;

/*- (void) drawRect: (float)_x
{
    NSLog(@"%.2f", _x);
}*/

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);// | GL_DEPTH_BUFFER_BIT);
//    NSLog(@"mine--");
    
    if ( 0 ) {
    // Render the object again with ES2
        glUseProgram(_program);
    
        glBindVertexArrayOES(_vertexArray);
        glDrawArrays(GL_TRIANGLES, 0, 36);
    } else {
        float width = 0.01f;
        float markersize = 0.08f;
        //markersize = 0.15f;
        float rate =12.0f / 16.0f;
        
        CGFloat w = self.view.bounds.size.width;
        CGFloat h = self.view.bounds.size.height;
        
        drawRect(-1, -1, 2, 2, 1.0f, 1.0f, 1.0f);
        
        drawRect(-1, 1.0f - 2 * lastY / h - 0.5f * width, 2.0f, width, 0.95f, 0.95f, 0.95f);
        drawRect(1.0f - markersize,1.0f - 2*lastY/ h-0.5f*markersize*rate,markersize,markersize*rate,1.0f,0.0f,0.0f);


        
        //NSLog(@"mine");
//        static int toggle = 0;
    
//        if (toggle == 0)
        
        // MARKERS
        drawRect(-1.0f,-1.0f, 2.0f ,0.2f,0.0f,0.0f,0.0f);
        drawRect(-1.0f,0.8f, 2.0f ,0.2f,0.0f,0.0f,0.0f);
        
        
        drawRect(-1,-1,markersize,markersize*rate,1.0f,0.0f,0.0f);
        drawRect(1-markersize,-1,markersize,markersize*rate,0.0f,1.0f,0.0f);
        drawRect(-1,1-markersize*rate,markersize,markersize*rate,0.0f,0.0f,1.0f);
        drawRect(1-markersize,1-markersize*rate,markersize,markersize*rate,1.0f,1.0f,1.0f);
        
        drawRect(-1 + (2-markersize)*0.9f,1-markersize*rate,markersize,markersize*rate,0.0f,0.0f,1.0f);
        drawRect(-1 + (2-markersize)*0.9f,-1,markersize,markersize*rate,0.0f,0.0f,1.0f);
        drawRect(0-markersize*0.5f,-1 + (2-markersize)*0.96f,markersize,markersize*rate,0.0f,1.0f,0.0f);
    }
}

#pragma mark -  OpenGL ES 2 shader compilation

- (BOOL)loadShaders
{
    GLuint vertShader, fragShader;
    NSString *vertShaderPathname, *fragShaderPathname;
    
    // Create shader program.
    g_program = _program = glCreateProgram();
    
						// Create and compile vertex shader.
    vertShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"vsh"];
    if (![self compileShader:&vertShader type:GL_VERTEX_SHADER file:vertShaderPathname]) {
        NSLog(@"Failed to compile vertex shader");
        return NO;
    }
    
    // Create and compile fragment shader.
    fragShaderPathname = [[NSBundle mainBundle] pathForResource:@"Shader" ofType:@"fsh"];
    if (![self compileShader:&fragShader type:GL_FRAGMENT_SHADER file:fragShaderPathname]) {
        NSLog(@"Failed to compile fragment shader");
        return NO;
    }
    
    // Attach vertex shader to program.
    glAttachShader(_program, vertShader);
    
    // Attach fragment shader to program.
    glAttachShader(_program, fragShader);
    
    // Bind attribute locations.
    // This needs to be done prior to linking.
    glBindAttribLocation(_program, GLKVertexAttribPosition, "vPosition");
//    glBindAttribLocation(_program, GLKVertexAttribNormal, "normal");
    
    // Link program.
    if (![self linkProgram:_program]) {
        NSLog(@"Failed to link program: %d", _program);
        
        if (vertShader) {
            glDeleteShader(vertShader);
            vertShader = 0;
        }
        if (fragShader) {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (_program) {
            glDeleteProgram(_program);
            _program = 0;
        }
        
        return NO;
    }
    
    // Get uniform locations.
    uniforms[UNIFORM_MODELVIEWPROJECTION_MATRIX] = glGetUniformLocation(_program, "modelViewProjectionMatrix");
    uniforms[UNIFORM_NORMAL_MATRIX] = glGetUniformLocation(_program, "normalMatrix");
    
    // Release vertex and fragment shaders.
    if (vertShader) {
        glDetachShader(_program, vertShader);
        glDeleteShader(vertShader);
    }
    if (fragShader) {
        glDetachShader(_program, fragShader);
        glDeleteShader(fragShader);
    }
    
    return YES;
}

- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type file:(NSString *)file
{
    GLint status;
    const GLchar *source;
    
    source = (GLchar *)[[NSString stringWithContentsOfFile:file encoding:NSUTF8StringEncoding error:nil] UTF8String];
    if (!source) {
        NSLog(@"Failed to load vertex shader");
        return NO;
    }
    
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &source, NULL);
    glCompileShader(*shader);
    
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        NSLog(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif
    
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0) {
        glDeleteShader(*shader);
        return NO;
    }
    
    return YES;
}

- (BOOL)linkProgram:(GLuint)prog
{
    GLint status;
    glLinkProgram(prog);
    
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program link log:\n%s", log);
        free(log);
    }
#endif
    
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

- (BOOL)validateProgram:(GLuint)prog
{
    GLint logLength, status;
    
    glValidateProgram(prog);
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        NSLog(@"Program validate log:\n%s", log);
        free(log);
    }
    
    glGetProgramiv(prog, GL_VALIDATE_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    NSLog(@"validateProgram");
    return YES;
}
- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
//    NSLog(@"touchBegan");
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    bool bWithPrediction = false;
    
    if (!bWithPrediction) {
        UITouch *touch_last = [[event allTouches] allObjects].lastObject;
    //    UITouch *touch_any = [[event allTouches] anyObject];
        float x, y;
        CGPoint p = [touch_last locationInView:self.view];
        x = p.x;
        y = p.y;
        
    //    [event coalescedTouchesForTouch];
        
    //    NSLog(@" nr(%zd) any(%.2f) last(%.2f)", [[event allTouches] allObjects].count, [touch_any locationInView:self.view].x, [touch_last locationInView:self.view].x );
    //    NSLog(@"lastX(%.2f) curX(%.2f)", lastX, x);
//        NSLog(@"curX(%.2f %.2f)", x, y);
        lastX = x;
        lastY = y;
    
    } else
    // with prediction
    {
        UITouch *touch = [touches anyObject];
        if ([event coalescedTouchesForTouch:touch]) {
            NSArray *coalescedTouches = [NSArray arrayWithArray: [event coalescedTouchesForTouch:touch]];
//            NSLog(@"Coalesced Touches: %lu",(unsigned long)[coalescedTouches count]);
            for (UITouch*coalescedTouch in coalescedTouches) {
                CGPoint locationInView = [coalescedTouch locationInView:self.view];
//                NSLog(@"(%.2f %.2f)", locationInView.x, locationInView.y);
            }
            
            for (UITouch* predict in [event predictedTouchesForTouch:touch]){
                CGPoint locationInView = [predict locationInView:self.view];
//                NSLog(@"P(%.2f %.2f)", locationInView.x, locationInView.y);
                lastX = locationInView.x;
                lastY = locationInView.y;
            }

            
        }
    }
    NSLog(@"---- y(%.2f)", lastY);
}

-(void) touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
//    NSLog(@"touchesEnded");
}
@end
